<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description>Como usuario deseo comprar X productos

-Ingresar con un usuario
-Agregar X Productos N veces al carrito
-Llenar datos para la compra de produtos
-Verificar la compra
-Comprar</description>
   <name>TSBK-003 CreateUsersAndBuyProduct</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>aa2910e6-6d1c-47ec-96c9-378945412eaf</testSuiteGuid>
   <testCaseLink>
      <guid>bc58b198-884f-419d-896d-0901862cee5d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Home Page/TCBK-0001 Go Page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>63dfb570-6250-4bb6-8bea-2f8403269c24</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BindingDataTC/TCBK-00020 Create users and buy produts</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>c8674ab6-91e7-4612-baf7-e5f809474102</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DTBK-001 Users</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>c8674ab6-91e7-4612-baf7-e5f809474102</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>User</value>
         <variableId>0dee0d61-61f9-4376-b0ff-843b6bc62a93</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>c8674ab6-91e7-4612-baf7-e5f809474102</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Password</value>
         <variableId>357ee497-310e-4158-9801-dc55277dff68</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>e23f5006-9ff2-4145-a835-00dd1e07289d</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
