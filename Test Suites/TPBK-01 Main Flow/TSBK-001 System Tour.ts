<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description>Flujo que permitira observar desde un crear usuario y login de usuario, en el home page si las redirecciones estan correctas, también la visualización de los productos en categorias completas o por categorias.</description>
   <name>TSBK-001 System Tour</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>44fdb26c-aea9-49ae-9a9d-ff35b2f0ed12</testSuiteGuid>
   <testCaseLink>
      <guid>12252665-3af2-4be7-ad3f-57799fa86f27</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Home Page/TCBK-0001 Go Page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>57c7adff-3e6f-4ed2-b8ba-36cd55136fc9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Home Page/TCBK-0002 SingUp</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>a49e7afd-2efc-4977-8027-d9328f8ac754</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>bba4e380-bfe9-49f2-8985-e06a90648057</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>d8588534-11e0-4af8-8d1a-40f98675b6b3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Home Page/TCBK-0003 Login</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7163655c-302d-47d8-9864-f1b9469f006e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Home Page/TCBK-0004 List Products</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5cec7dd5-26fd-46da-a46e-d882505073e8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Home Page/TCBK-0005 Write in Contact</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>69cb3830-4fee-4970-b7cc-9323187e658d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Home Page/TCBK-0006 ViewAboutUs</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a186743e-ab00-45cb-95d1-cb2f82a5e131</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Home Page/TCBK-0007 LogOut</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
