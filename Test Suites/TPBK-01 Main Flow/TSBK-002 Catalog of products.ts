<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description>Si los productos se muestran al ingresar, y si pueden cada uno de estos agregar al carrito de compra.
-Ingresar a cada producto
-Agregar cada producto al carrito
-Comprar todos los productos agregados al carrito</description>
   <name>TSBK-002 Catalog of products</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>15c0ffd8-1ae9-49c2-90e8-ec5a16909747</testSuiteGuid>
   <testCaseLink>
      <guid>618f03dd-7081-4f24-a1fa-9218420dfc79</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Home Page/TCBK-0001 Go Page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5ede0c2e-adab-4ab3-b7c5-2d6f4a9333e7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Home Page/TCBK-0003 Login</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>953cdbd0-b066-49a5-bb21-1d5fd8af0ecf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Products/TCBK-00010 AddProductsToCart</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>56a2cd07-3005-4d2d-b91d-9fe0c052c8db</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Products/TCBK-00011 BuyCartProducts</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>1d64de38-3db8-4685-b331-de33166529cc</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>af5c1be3-a465-407e-851f-cc63511d4a95</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>23277d08-edea-4c10-8fb1-007a44b6e9a5</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>777dd357-0d11-4a29-af22-1a41219c6f95</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>7f552a61-6588-4e46-9e46-13c4d2ffcfad</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>96000c3c-ce17-42ff-865f-9a4a3ebced2e</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
