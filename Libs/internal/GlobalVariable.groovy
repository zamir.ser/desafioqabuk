package internal

import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.main.TestCaseMain


/**
 * This class is generated automatically by Katalon Studio and should not be modified or deleted.
 */
public class GlobalVariable {
     
    /**
     * <p></p>
     */
    public static Object User
     
    /**
     * <p></p>
     */
    public static Object Password
     
    /**
     * <p></p>
     */
    public static Object LinkWebPage
     
    /**
     * <p></p>
     */
    public static Object BindingData
     
    /**
     * <p></p>
     */
    public static Object ContactName
     
    /**
     * <p></p>
     */
    public static Object ContactEmail
     
    /**
     * <p></p>
     */
    public static Object ContactMessaje
     
    /**
     * <p></p>
     */
    public static Object ProductsNumbers
     
    /**
     * <p></p>
     */
    public static Object ProductsNumbersForPage
     

    static {
        try {
            def selectedVariables = TestCaseMain.getGlobalVariables("default")
			selectedVariables += TestCaseMain.getGlobalVariables(RunConfiguration.getExecutionProfile())
            selectedVariables += TestCaseMain.getParsedValues(RunConfiguration.getOverridingParameters())
    
            User = selectedVariables['User']
            Password = selectedVariables['Password']
            LinkWebPage = selectedVariables['LinkWebPage']
            BindingData = selectedVariables['BindingData']
            ContactName = selectedVariables['ContactName']
            ContactEmail = selectedVariables['ContactEmail']
            ContactMessaje = selectedVariables['ContactMessaje']
            ProductsNumbers = selectedVariables['ProductsNumbers']
            ProductsNumbersForPage = selectedVariables['ProductsNumbersForPage']
            
        } catch (Exception e) {
            TestCaseMain.logGlobalVariableError(e)
        }
    }
}
