import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



public class HomePageObjects {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */
	@Given("I want to write a step with (.*)")
	def I_want_to_write_a_step_with_name(String name) {
		println name
	}

	@When("I check for the (\\d+) in step")
	def I_check_for_the_value_in_step(int value) {
		println value
	}

	@Then("I verify the (.*) in step")
	def I_verify_the_status_in_step(String status) {
		println status
	}





	//HEARDERS
	def static TestObject HomePageRedirect = {
		return findTestObject('Object Repository/Home Page/a_Home')
	};

	def static TestObject ContactPageRedirect = {
		return findTestObject('Object Repository/Home Page/a_Contact')
	}

	def static TestObject AboutUsModalRedirect = {
		return findTestObject('Object Repository/Home Page/a_About us')
	};

	def static TestObject CartPageRedirect = {
		return findTestObject('Object Repository/Home Page/a_Cart');
	};

	def static TestObject LogInModalRedirect = {
		return findTestObject('Object Repository/Home Page/a_LogIn')
	};

	def static TestObject SingUpModalRedirect = {
		return findTestObject('Object Repository/Home Page/a_SignUp')
	};
	
	public static TestObject SSingUpModalRedirect = {
		TestObject to = findTestObject('Object Repository/Home Page/a_SignUp');
		return to;
	}


	//PRODUCTS
	def static TestObject GetAllProducts = {
		return findTestObject('Object Repository/Home Page/a_Categories')
	};

	def static TestObject GetPhonesProducts = {
		return findTestObject('Object Repository/Home Page/a_Phones')
	};

	def static TestObject GetLaptopsProducts = {
		return findTestObject('Object Repository/Home Page/a_Laptops')
	}

	def static TestObject GetMonitorsProduts = {
		return findTestObject('Object Repository/Home Page/a_Monitors')
	};




}