import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

//Objects
TestObject a_singUpModalRedirect = findTestObject('Object Repository/Home Page/a_SignUp')
TestObject i_usernameSingUp = findTestObject('Object Repository/LogIn SingUp Modal/input_UsernameSingUp')
TestObject i_passwordSingUp = findTestObject('Object Repository/LogIn SingUp Modal/input_PasswordSingUp')
TestObject btn_singUp = findTestObject('Object Repository/LogIn SingUp Modal/btn_SignUp')
TestObject a_logInModalRedirect = findTestObject('Object Repository/Home Page/a_LogIn')
TestObject i_usernameLogIn = findTestObject('Object Repository/LogIn SingUp Modal/input_UsernameLogIn')
TestObject i_passwordLogIn = findTestObject('Object Repository/LogIn SingUp Modal/input_PasswordLogIn')
TestObject btn_LogIn = findTestObject('Object Repository/LogIn SingUp Modal/btn_LogIn')
TestCase tc_addOneProductToCart = findTestCase('Products/TCBK-00012 AddOneProductToCart')
TestCase tc_BuyCartProducts = findTestCase('Products/TCBK-00011 BuyCartProducts')
TestCase tc_logOutSession = findTestCase('Home Page/TCBK-0007 LogOut')

WebUI.waitForElementClickable(a_singUpModalRedirect, 3)
//Clic on SingUp to open modal
WebUI.click(a_singUpModalRedirect)

//Set User & Password to User Profile
WebUI.setText(i_usernameSingUp, UserText)
WebUI.setText(i_passwordSingUp, PasswordText)

WebUI.click(btn_singUp)
WebUI.waitForPageLoad(1)


WebUI.waitForElementClickable(a_logInModalRedirect, 3)
//Clic on SingUp to open modal
WebUI.click(a_logInModalRedirect)

//Set User & Password to User Profile
WebUI.setText(i_usernameLogIn, UserText)
WebUI.setText(i_passwordLogIn, PasswordText)

//LogIn with new user
WebUI.click(btn_LogIn)

//Calling TestCase AddOneProductToCart, BuyCart, and Log Out Session
WebUI.callTestCase(tc_addOneProductToCart, [('ProductNumber') : 4], FailureHandling.STOP_ON_FAILURE)
WebUI.callTestCase(tc_BuyCartProducts, [('NameText') : 'Zamir Enriquez QA', ('CountryText') : 'Bolivia', ('CityText') : 'La Paz'
        , ('CreditCardText') : 5789982323, ('MonthText') : 12, ('YearText') : 2024], FailureHandling.STOP_ON_FAILURE)
WebUI.callTestCase(tc_logOutSession, [:], FailureHandling.STOP_ON_FAILURE)

