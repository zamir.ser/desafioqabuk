import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

//Objects
TestObject a_GetCategoriesRedirect = findTestObject('Object Repository/Home Page/a_Categories');
TestObject a_GetAllPhonesRedirect = findTestObject('Object Repository/Home Page/a_Phones');
TestObject a_GetAllLaptopsRedirect = findTestObject('Object Repository/Home Page/a_Laptops');
TestObject a_GetAllMonitorsRedirect = findTestObject('Object Repository/Home Page/a_Monitors');

//List Products
WebUI.click(a_GetAllPhonesRedirect);
WebUI.click(a_GetAllLaptopsRedirect);
WebUI.click(a_GetAllMonitorsRedirect);
WebUI.click(a_GetCategoriesRedirect);
