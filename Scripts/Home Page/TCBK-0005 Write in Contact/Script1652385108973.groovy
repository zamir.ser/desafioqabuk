import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys


//Objects
TestObject i_ContactPageRedirect = findTestObject('Object Repository/Home Page/a_Contact');
TestObject i_ContactEmail = findTestObject('Contact Page/input_emailContact');
TestObject i_ContactName = findTestObject('Contact Page/input_NameContact');
TestObject i_ContactMessaje = findTestObject('Contact Page/input_MessageContact');
TestObject btn_SendMessaje = findTestObject('Object Repository/Contact Page/btn_SendMessajeContact');

//Go to Contact Page and send messaje
WebUI.click(i_ContactPageRedirect);


if(!GlobalVariable.BindingData){
	WebUI.setText(i_ContactEmail, GlobalVariable.ContactEmail);
	WebUI.setText(i_ContactName, GlobalVariable.ContactName);
	WebUI.setText(i_ContactMessaje, GlobalVariable.ContactMessaje);	
}else {
	//Add Data
}

WebUI.click(btn_SendMessaje);