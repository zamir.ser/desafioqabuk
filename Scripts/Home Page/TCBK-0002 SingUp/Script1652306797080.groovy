import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

//Objects
TestObject a_singUpModalRedirect = findTestObject('Object Repository/Home Page/a_SignUp')
TestObject i_usernameSingUp = findTestObject('Object Repository/LogIn SingUp Modal/input_UsernameSingUp')
TestObject i_passwordSingUp = findTestObject('Object Repository/LogIn SingUp Modal/input_PasswordSingUp')
TestObject btn_singUp = findTestObject('Object Repository/LogIn SingUp Modal/btn_SignUp')

//Clic on SingUp to open modal
WebUI.click(a_singUpModalRedirect);

//Set User & Password to User Profile
if(!GlobalVariable.BindingData) {
	//Set data from GlobalVariable
	WebUI.setText(i_usernameSingUp, GlobalVariable.User);
	WebUI.setText(i_passwordSingUp, GlobalVariable.Password);
}else {
	//Add data
	
}
WebUI.click(btn_singUp);
WebUI.waitForPageLoad(1);