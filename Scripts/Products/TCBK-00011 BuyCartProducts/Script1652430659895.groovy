import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

//Objects
TestObject btn_ok = findTestObject('Object Repository/Cart Page/btn_Ok_RedirectHomePAge')
TestObject btn_placeOrder = findTestObject('Object Repository/Cart Page/btn_PlaceOrderCart')
TestObject btn_purchase = findTestObject('Object Repository/Cart Page/btn_PurchaseModalCart')
TestObject i_cityText = findTestObject('Object Repository/Cart Page/input_CityModalCart')
TestObject i_countryText = findTestObject('Object Repository/Cart Page/input_CountryModalCart')
TestObject i_creditCardText = findTestObject('Object Repository/Cart Page/input_CreditCardModalCart')
TestObject i_monthText = findTestObject('Object Repository/Cart Page/input_MonthModalCart')
TestObject i_nameText = findTestObject('Object Repository/Cart Page/input_NameModalCart')
TestObject i_yearText = findTestObject('Object Repository/Cart Page/input_YearModalCart')
TestObject a_cartPageRedirect = findTestObject('Object Repository/Home Page/a_Cart')

//Start in Cart
WebUI.click(a_cartPageRedirect)

//Continue Place Order
WebUI.click(btn_placeOrder)

//Set Data on Modal
WebUI.setText(i_nameText, NameText)
WebUI.setText(i_countryText, CountryText)
WebUI.setText(i_cityText, CityText)
WebUI.setText(i_creditCardText, CreditCardText.toString())
WebUI.setText(i_monthText, MonthText.toString())
WebUI.setText(i_yearText, YearText.toString())

//Purchase now
WebUI.click(btn_purchase)

//Out Cart Page
WebUI.click(btn_ok)


