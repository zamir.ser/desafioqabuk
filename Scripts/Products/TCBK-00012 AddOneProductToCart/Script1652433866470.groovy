import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys


import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement


//Objects
TestObject a_HomePageRedirect = findTestObject('Object Repository/Home Page/a_Home')
TestObject btn_nextPage = findTestObject('Object Repository/Home Page/btn_nextPageProducts')
WebDriver driver = DriverFactory.getWebDriver()

//Go Home Page
WebUI.click(a_HomePageRedirect)
//Search product and convert to TestObject for click and redirect productPage
String xPathAct = ('//div[@id=\'tbodyid\']/div[' + ProductNumber.toString()) + ']/div/div/h4/a'
TestObject productRedirect = new TestObject("product")
productRedirect.addProperty('xpath', ConditionType.EQUALS, xPathAct)
WebUI.click(productRedirect)
//Wait 1 second
WebUI.delay(1)

//Add Product in the Cart
String xPatchProductBtn = "//a[@onclick='addToCart(" + ProductNumber.toString() +")']"
TestObject btnBuyProduct = new TestObject("buyProduct")
btnBuyProduct.addProperty('xpath', ConditionType.EQUALS, xPatchProductBtn)
WebUI.click(btnBuyProduct)