# DesafioQABuk Zamir Enriquez



## Agradezco la oportunidad estoy seguro que seguiremos adelante
El proyecto actual contiene el codigo desarrollado para ser utilizado con el framework de Katalon Studio.
Realice el desarrollo de 3 TestSuites y 11 TestCases.
El presente proyecto cuenta con un documento de tipo .xlsx para el uso de BingData en el proyecto.

## Consideraciones

Katalon Studio v8.0.5
Java JRE 1.8.0
Java JDK 8.3

## Para descargar el código

```
cd repositorio
git init
git clone https://gitlab.com/zamir.ser/desafioqabuk.git
git pull
git checkout main
```

## Descarga Katalon Studio

- [ ] [Descarga la versión 8.0.5](https://www.katalon.com/download/)

## Video Explicativo de las pruebas automatizadas y código fuente

- [ ] [Ingresa para ver o descargar el video](https://www.icloud.com/iclouddrive/0a4rzIWHW7BKChiq3vdIw9Dnw)

## Cualquier duda o consulta estoy atento en mi contacto

- [ ] [WhatsApp](https://api.whatsapp.com/send/?phone=59175142997&text&app_absent=0)
- [ ] [Mail](mailto:zamir.ser@gmail.com)

